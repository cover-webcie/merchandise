<?php
require 'include/init.php';
require 'include/cart.php';
require 'include/session.php';

function format_options($item)
{
	$options = array();

	foreach ($item->options as $option => $value)
		$options[] = sprintf('<dt>%s</dt><dd>%s</dd>',
			$item->product->options->{$option}->label,
			$item->product->options->{$option}->values->{$value}->label);

	return implode($options);
}

$title = 'Cart';

$cart = get_cart();

$user = get_cover_session();

if ($user)
	$user->naam = implode(' ', array_filter(array($user->voornaam, $user->tussenvoegsel, $user->achternaam)));

$content = render_template('templates/cart.phtml', compact('cart', 'user'));

echo render_template('templates/layout.phtml', compact('title', 'content'));
