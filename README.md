# Merchandise

Simple backend for Cover merch shop.

## Configuration

NGINX:

    location /images/products/ {
        alias /phoenix-storage/srv/merchandise/product-images/;
    }

Apache2:

    Alias /images/products /phoenix-storage/srv/merchandise/product-images

Example: products.json:

    {
        "cover-sweatpants": {
            "title": "Cover sweatpants",
            "description": "In style to the gym, or maybe even run the Batavierenrace together with the Conditie? Strolling around Intro-camp in chillpants, or maybe on a late morning with a hangover after a night in town with the Actie? Who would not wan't such a perfect pants!",
            "images": {
                "small": "sweatpants.jpg",
                "large": ["sweatpants-full.jpg"]
            },
            "price": 23.00,
            "details": {
                "Model": "FR600/FR601",
                "Delivery": "Approx. 10 days after ordering.",
                "Color": "Black",
                "Print": "Printed, white on the side."
            },
            "options": {
                "sex": {
                    "label": "Sex",
                    "values": {
                        "m": {"label": "Male"},
                        "f": {"label": "Female"}
                    }
                },
                "size": {
                    "label": "Size",
                    "values": {
                        "s": {"label": "S"},
                        "m": {"label": "M"},
                        "l": {"label": "L"},
                        "xl": {"label": "XL"},
                        "xxl": {"label": "XXL"}
                    }
                }
            }
        },
        "cover-scarf": {
            "title": "Cover scarf (limited)",
            "description": "The Cover scarf is perfect for that strong winter. There are only a few more scarfs available, so be quick about it!",
            "images": {
                "small": "scarf.jpg",
                "large": ["scarf-2.jpg", "scarf-1.jpg", "scarf-0.jpg"]
            },
            "price": 11.00,
            "details": {
                "Delivery": "Can be directly obtained from the Cover room.",
                "Color": "Red",
                "Print": "Stiched, white on the end."
            }
        },
    }
