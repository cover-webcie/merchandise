<?php

function http_json_request($url, array $data = null)
{
	$options = array(
		'http' => $data !== null
			? array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($data))
			: array(
				'method'  => 'GET'
			)
	);

	$context  = stream_context_create($options);

	$response = file_get_contents($url, false, $context);

	if (!$response)
		throw new Exception('No response');

	$data = json_decode($response);

	if ($data === null)
		throw new Exception('Response could not be parsed as JSON: <pre>' . htmlentities($response) . '</pre>');

	return $data;
}

function get_cover_session()
{
	static $session = null;

	// Is there a local session id?
	if (!empty($_COOKIE['cover_sd_session_id']))
		$session_id = $_COOKIE['cover_sd_session_id'];

	// Is there a cover website global session id available?
	elseif (!empty($_COOKIE['cover_session_id']))
		$session_id = $_COOKIE['cover_session_id'];

	// If both not, bail out. I have no place else to look :(
	else
		return null;

	if ($session !== null)
		return $session;

	$data = array('session_id' => $session_id);

	$response = http_json_request('https://www.svcover.nl/api.php?method=session_get_member', $data);

	if (!$response->result)
		$session = false;

	return $session = $response->result;
}

function cover_session_logged_in()
{
	return get_cover_session() !== false;
}

function cover_session_create($email, $password, &$error = null)
{
	$application = 'Standaarddocumenten';

	$data = compact('email', 'password', 'application');

	$response = http_json_request('https://www.svcover.nl/api.php?method=session_create', $data);

	if ($response->result) {
		setcookie('cover_sd_session_id', $response->result->session_id, time() + 7 * 24 * 3600, '/');
		return true;
	}
	else {
		$error = $response->error;
		return false;
	}
}

function cover_session_destroy()
{
	if (!isset($_COOKIE['cover_sd_session_id']))
		return null;

	$data = array('session_id' => $_COOKIE['cover_sd_session_id']);

	$response = http_json_request('https://www.svcover.nl/api.php?method=session_destroy', $data);

	setcookie('cover_sd_session_id', '', time() - 24 * 3600, '/');
}

function cover_session_status($login_link, $logout_link)
{
	$session = get_cover_session();

	if (!$session)
		$content = '
		<a id="login-link" class="button" href="' . $login_link . '">Log in</a>
		<form action="' . $login_link . '" method="post" class="login">
			<input type="email" name="email" placeholder="e-mail address">
			<input type="password" name="password" placeholder="password">
			<button type="submit">Log in</button>
		</form>';
	else
		$content = sprintf('Logged in as %s.
			<a class="button" href="%s">Log out</a>',
			$session->voornaam, 
			!empty($_COOKIE['cover_sd_session_id'])
				? $logout_link
				: 'https://www.svcover.nl/dologout.php?referrer=' . rawurlencode('https://sd.svcover.nl/')
			);

	return sprintf('<div class="session">%s</div>', $content);
}

function cover_session_get_committees()
{
	static $committees;

	if ($committees !== null)
		return $committees;
	
	$session = get_cover_session();

	if (!$session)
		return array();

	$response = http_json_request('https://www.svcover.nl/api.php?method=get_committees&member_id=' . $session->id);

	$committees = array();

	foreach ($response->result as $committee)
		$committees[] = strtolower($committee);

	return $committees;
}

function cover_session_in_committee($committee)
{
	return in_array(strtolower($committee), cover_session_get_committees());
}
