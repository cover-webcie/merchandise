<?php
require 'include/init.php';
require 'include/cart.php';


if (!($product = get_product($_GET['product'])))
{
	header('Status: 404 Not Found');
	die('Product not found');
}

$title = $product->title;

$content = render_template('templates/product.phtml', compact('product'));

echo render_template('templates/layout.phtml', compact('title', 'content'));
