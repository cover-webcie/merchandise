<?php

require 'include/init.php';
require 'include/cart.php';

class FormException extends Exception
{
	public $fields;

	public function __construct($message, array $fields)
	{
		parent::__construct($message);

		$this->fields = $fields;
	}
}

function array_get(array $fields, array $data)
{
	$subset = array();

	foreach ($fields as $field)
		$subset[$field] = isset($_POST[$field])
			? $_POST[$field]
			: null;

	return $subset;
}

function run()
{
	$action = isset($_POST['action']) ? $_POST['action'] : '';

	$result = array();

	switch ($action)
	{
		case 'add':
			add_to_cart($_POST['product'], !empty($_POST['options']) ? $_POST['options'] : array());
			$return_to = 'index.php?status=added';
			break;
			
		case 'remove':
			remove_from_cart($_POST['item']);
			$return_to = 'cart.php';
			break;

		case 'order':
			$fields = array('name', 'address', 'postal_code', 'place', 'email_address', 'bank_account', 'agree', 'remarks');
			$result = order_cart(array_get($fields, $_POST));
			$return_to = 'index.php?status=ordered';
			break;
	}

	return array_merge(array(
		'cart_item_count' => get_cart_item_count(),
		'cart_price' => get_cart_price()
		), $result);
}

header('Content-Type: application/json');

try {
	echo json_encode(array_merge(array('error' => false), run()));
}
catch (FormException $e) {
	echo json_encode(array('error' => true, 'message' => $e->getMessage(), 'fields' => $e->fields));
}
catch (Exception $e) {
	echo json_encode(array('error' => true, 'message' => $e->getMessage()));
}
